import React from 'react';
import HeaderNav from "../Navigation/HeaderNav/HeaderNav";
import './Layout.css';

const Layout = ({children}) => {
	return (
		<div className="container">
			<HeaderNav/>
			<main>{children}</main>
		</div>
	);
};

export default Layout;