import React, {useEffect, useState} from 'react';
import axiosApi from "../../axiosApi";
import withLoader from "../../hoc/withLoader";

const Page = ({match}) => {
	const [page, setPage] = useState([]);

	useEffect(() => {
		const fetchData = async () => {
			const response = await axiosApi.get('/pages/' + match.params.page + '.json');
				setPage(response.data);
		};

		fetchData().catch(console.error);
	}, [match.params.page]);

	return page && (
		<>
			<h2>{page.title}</h2>
			<p>{page.content}</p>
		</>
	);
};

export default withLoader(Page, axiosApi);